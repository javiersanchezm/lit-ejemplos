//Importar funciones de la librería lit-html
import {html, render} from 'lit-html.js';

const mainNode = document.getElementById('main-0'); //Nodo
//Definir el template como una función que recibe datos y regresa
//un objeto TemplateResult
const template0 = name => {
    return html`<p>Hello ${name}</p>`;
};
render(template0('Silvia'), mainNode); //Renderizar el template
render(template0('Benjamin'), mainNode); //Renderizar con diferentes datos


//Texto: Agregar texto de propiedades de un objeto (Obligatorio usar arrow function)
const mainNode1 = document.getElementById('main-1');
let someText = 'Lorem ipsum';
let user = { name : 'JEN', firstName: 'MONROE' };
const template1 = user => {
    return  html`<p>${user.name} - ${user.firstName} - ${someText}</p>`;
}
render(template1(user), mainNode1);


//Atributo: Una expresión que devuelve un texto se puede utilizar como valor
//de atributo, asignarle valores a una clase para que tome algunos estilos
const mainNode2 = document.getElementById('main-2');
let classList = ['main', 'required', 'modern'].join(' ');
const template2 = () => {
    return html`<div class=${classList}>Stylish text.</div>`;
}
render(template2(), mainNode2);




//Atributo: Ocultar o mostrar html/atributo de acuerdo al valor booleano
const mainNode3 = document.getElementById('main-3');
let formWithErrors = true;
const template3 = formWithError => {
    return  html`<input type="submit" ? disabled=${formWithError} value="Submit">
            <span ?hidden=${!formWithError}>Form has errors!</span>`;
}
render(template3(formWithErrors), mainNode3);



//Asignar valores a un atributo de la etiqueta custom-list
const users = ['Diego', 'Ana', 'Laura', 'Piero'];
const customList = document.getElementById('user-list');
const res = html`<custom-list items=${users} id="user-list"></custom-list>`;
render(res, customList);






//Evento: Una expresión puede ser el controlador de un evento, eisten varias alternativas
const clickHandler = {
    handleEvent(e) { console.log('clicked!'); }
};

//Función de controlador global: Se resuelve en una función global que manejará el evento
const mainNode4 = document.getElementById('main-4');
const template40 = (handleClick) => {
    return html`<button @click=${handleClick}>Click Me!</button>`;
}
render(template40(clickHandler), mainNode4);

//Función en línea: La expresión se resuelve en una función en línea.
const mainNode5 = document.getElementById('main-5');
const template41 = () => html`<button @click=${()=>console.log('clicked')}>Click Mee!</button>`;
render(template41(), mainNode5);

//Enlace de eventos a una función de instancia
//html`<button @click=${this.handleClick}>Click Me!</button>`;

//Enlace de eventos al objeto de escucha
//html`<button @click=${clickHandler}>Click Me!</button>`;





//Elemento de nodo HTML: La expresión puede devolver un nodo DOM, crea elemento y lo agrega
const mainNode6 = document.getElementById('main-6');
const h1 = document.createElement('h1');
h1.textContent = 'Chapter 1';
const page = html`${h1}<p>Once upon a time...</p>`;
render(page, mainNode6);

//TemplateResult: La expresión puede ser otro objeto. Esto hace posible tener una
//composición de plantillas anidadas.
const mainNode7 = document.getElementById('main-7');
const header = html`<h1>Chapter 2</h1>`;
const article = html`<article>${header}<p>Once upon a time...</p></article>`;
render(article, mainNode7);






//Iterar objetos. Expresiones que devuelven una matriz o iterables de objetos TemplateResult
const mainNode8 = document.getElementById('main-8');
const items = [1, 2, 3];
const listItems = items.map(i => html`<li>${2 * i}</li>`);
const template = html`<ul>${listItems}</ul>`;
render(template, mainNode8);





//Promesa: La expresión puede devolver una promesa que debe resolverse devolviendo un valor
//viculante válido
const mainNode9 = document.getElementById('main-9');
let page1 = '';
const text = fetch('https://jsonplaceholder.typicode.com/posts/1')
            .then(response => response.json())
            .then(resp =>
                {
                    page1 = html`<p>${resp.title}-${resp.body}-${resp.id}</p>`;
                    render(page1, mainNode9);
                });



//Composición: Crear plantillas usando otras plantilla, permite:
//-Crear una plantilla compleja usando plantillas más simples
//-Refactorizar una plantila compleja sumergiéndola en plantillas simples
//-Reutilización de plantillas (El uso de módulos JavaScript facilita mucho la reutilización)
/* const header1 = data => html`
    <h1>${data.title}<h1>
    ${data.subtitle ? html`<h2>${data.subtitle}<h2>` : ''}`;

const main = data => html`<p>${makeParagraph(data.text)}</p>`;

const composedTemplate = data => html`
    ${header1(data)}
    ${main(data)}`;

render(composedTemplate(data), composition); */





//Condicionales: Uso del operador ternario, o estructuras condicionales, una plantilla
//puede tener partes que solo son visibles si se cumple una condición, o puede tener partes
//que se representan de diferentes maneras dependiendo de una o más condiciones.

/* // using ternary operator
const main = data => html`
    ${data.text ?
        html`<p>${data.text}</p>` :
        html`<img src=${data.image}></img>`}`;

// using if
const main = data => {
    if (data.text) {
        return html`<p>${data.text}</p>` :
    } else {
        return html`<img src=${data.image}></img>`;
    }
}

// using switch-case
const main = data => {
    switch (data.screenSize) {
    case 's':
        return html`<img src="${data.image}-small.png"></img>`;
    case 'm':
        return html`<img src="${data.image}-medium.png"></img>`;
    case 'l':
        return html`<img src="${data.image}-medium.png"></img>
                    <img src="${data.logo}.png"></img>`;
    default:
        return html`<p>${data.text}</p>`;
    }
} */






//Iteraciones: Una parte de la plantilla se repita con diferentes valores.
const weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Staurday', 'Sunday'];

//ciclos con arreglos
const mainNode10 = document.getElementById('main-10');
const template07 = (weekDays) => html`<ul>${weekDays.map((day) => html`<li>${day}</li>`)}</ul>`;
render(template07(weekDays), mainNode10);

//Declaración de ciclo (Es mejor si se actualiza del nodo DOM)
const mainNode11 = document.getElementById('main-11');
const itemTemplates = [];
for (const day of weekDays) {
    itemTemplates.push(html`<li>${day}</li>`);
}
const template08 = (itemTemplates) =>  html`<ul>${itemTemplates}</ul>`;
render( template08(itemTemplates), mainNode11 );