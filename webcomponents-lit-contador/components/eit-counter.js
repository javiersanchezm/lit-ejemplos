//Importar las clases LitElement, html y css de la librería lit
import { LitElement, html, css } from "lit";

//Importar módulos de wired-elements
import { WiredButton } from 'wired-elements/lib/wired-button.js';
import { WiredCard } from 'wired-elements/lib/wired-card.js';
import { WiredInput } from 'wired-elements/lib/wired-input.js';
import { WiredSlider } from 'wired-elements/lib/wired-slider.js';

//El componente se crea a través de LitElement
class EitCounter extends LitElement{

    static styles =[
        //host. Estilos a todo el componente, hace referencia a la tag padre
        css`
            :host {
                display: inline-block;
                /* padding: 1em;
                border: 1px solid yellow;
                background-color: yellow; */
            }

            h2{
                color: red;
            }

            .parrafo{
                color: blue;
                font-size: 1.5em;
            }

            wired-input{
                width: 60px;
                font-size: 1.5em;
            }

            wired-button{
                background-color: #8cf;
            }

            wired-button.decrement{
                background-color: #fcc;
            }

            wired-card{
                margin: 1em;
                padding: 1em;
            }
        `
    ];


    //Creación de propiedades, se declaran en un objeto, se pueden utilizar dentro
    //del template usando interpolación
    //nos permite probar los template reactivos
    static properties = {
        counter: {
            type: Number,
            //Se actualiza el valor en el template html y solo cambia ese valor
            reflect: true
        },
        quantity: { type:Number, },
    }

    //Inicializar variables que necesitemos en el constructor
    constructor(){
        super();
        this.counter = 10;
        this.quantity = 15;
    }

    //Método que define la vista del componente, actualiza el template de
    //manera reactiva
    render() {
        return html `
                <wired-card elevation="3">
                    <slot></slot>
                    <p class="parrafo">${this.counter}</p>

                    <!-- Las actualizaciones del contador incrementen/decrementen más rápido -->
                    <p>
                        <!-- Bindear la propiedad value del componente en lugar del atributo (.value)-->
                        <wired-input id="quantity" type="number" .value="${this.quantity}"></wired-input>
                    </p>

                    <p>
                        <wired-slider
                            value="10"
                            min="1"
                            max="20"
                            @change=${this.doChangeQuantity}
                        ></wired-slider>
                    </p>

                    <!-- Manejador de eventos con la siguiente sintaxis -->
                    <wired-button @click=${this.increment}>Incrementar</wired-button>
                    <wired-button @click=${this.decrement} class="decrement">Decrementar</wired-button>
                </wired-card>
        `;
    }

    /* get quantity(){
        //Acceder las etiquetas del template mediante el DOM
        //Se está desarrollando muy apegado al estándar de JavaScript
        return this.shadowRoot.getElementById('quantity').value;
    } */

    //Implementar manejador de evento, recibe un objeto evento
    doChangeQuantity( event ) {
        //Asignar el valor de slider a la propiedad quantity
        console.log( event.detail.value );
        this.quantity = event.detail.value;
    }

    increment() {
        this.counter+= parseInt(this.quantity);
    }

    decrement() {
        this.counter-= parseInt(this.quantity);;
    }
}

customElements.define("eit-counter", EitCounter);