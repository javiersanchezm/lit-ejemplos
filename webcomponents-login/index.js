//App principal

//Importar desde la librería lit
import { LitElement, html, css } from 'lit';
//Importr nuestro componente Login
import './Components/login-lit.js';

class AppLit extends LitElement{

    //Definición de variables y se puede usar de manera global
    static get properties() {
        return {
            success: { type: Boolean }
        };
    }

    //Darle una posición a nuestro componente
    static get styles(){
        return css`
            login-lit {
                display: flex;
                justify-content: center;
                margin-top: 100px;
            }
        `;
    }

    //Cada que se logee aparezca una nueva pantalla, realizarlo con operador ternario
    //Se recibe el evento para poder realizar una acción en especifica se debe tener
    //como una propiedad @sign y dentro va a realizar un acción
    render() {
        return html`
            ${this.success
                ? html`<h1>Bienvenidos</h1>`
                : html`<login-lit @sign="${ this._hiddenLogin }"></login-lit>`}
        `;
    }

    //Encargada de cambiar el valor de la propiedad success
    _hiddenLogin() {
        this.success = true;
    }
}

customElements.define('app-lit', AppLit);