
//Web component
import { LitElement, html, css } from "lit";

class LoginLit extends LitElement{

    //Agregar estilos, lo ofrece separando la estructura html y estilos css
    //Función estática
    static get styles(){
        return css`
            .container {
                border: 3px solid #007eaf;
                border-radius: 5px;
                width: 350px;
                height: 400px;
                text-align: center;
            }

            input {
                width: 90%;
                height: 30px;
                margin-top: 8vh;
                border: 1px solid #414141;
                border-top: 0px;
                border-radius: 2px;
            }

            button {
                width: 60%;
                height: 40px;
                background-color: #007eaf;
                color: white;
                border: none;
                border-radius: 6px;
                margin-top: 8vh;
            }

            button:hover{
                background-color: #005e83;
                cursor: pointer;
            }
        `;
    }

    //Renderiza los elementos LitElement, se crea la estructur básica de login
    render() {
        return html `
            <div class="container">
                <h2>Login LitElement</h2>
                <input id="email" type="email" placeholder="Escribe tu correo">
                <input id="password" type="password" placeholder="Escribe tu contraseña">

                <button @click="${ this._login }">Sign in</button>
            </div>
        `;
    }

    //Función privada para la lógica, cada que se de click en el botón
    _login() {
        //Acceder a los valores de los input
        //Hacemos referencia al contexto actual y al shadowRoot
        const email = this.shadowRoot.getElementById("email").value;
        const password = this.shadowRoot.querySelector("#password").value;

        //Validación para email y contraseña no vengan vacíos
        if( !!email && !!password){
            //Comunicación de un componente hijo a padre mediante eventos
            //Crear un evento Custimizado el primer param. es su nombre
            //El segundo es un objeto: 
            //  -Detail: Nor permite agregar dentro información
            //  -bubbles: Si queremos que el elemento se propague dento del
            //      componente local hacía los padres
            this.dispatchEvent( new CustomEvent('sign', {
                detail: { login: true },
                bubbles: true, composed: true
            }) );
        }
    }
}

//Definir el componente, nombre de la etiqueta
customElements.define('login-lit', LoginLit);