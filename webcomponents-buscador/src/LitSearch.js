import { html, LitElement } from 'lit';

export class LitSearch extends LitElement {

  static get properties() {
    return {
      data: { type: Array },
      result: { type: Array }
    };
  }

  constructor() {
    super();

    //Inicializar datos por defecto
    this.data = [
      { name: 'iPhone', value: '100' },
      { name: 'Xbox One', value: '100' },
      { name: 'Xbox Series X', value: '100' },
      { name: 'Play Station', value: '100' },
      { name: 'TV LG', value: '100' },
    ];

    this.result = this.data;
  }

  render() {
    return html`
      <h1>Buscador en LitElement</h1>
      <input @keyup=${this.filterData} type="text" id="form">
      <button @click=${this.filterData}>Buscar</button>

      <ul>
        ${this.result.map( elem => html`
          <li>${elem.name} - ${elem.value} </li>
        `)}
      </ul>
    `;
  }

  //filtrar cada item de lo que se escriba, se debe traer
  //lo que se está leyendo del input
  filterData() {
    //Cada componente de litElement está dentro de un shadowRoot
    //Para aislar de otros componentes y no choquen los estilos
    const input = this.shadowRoot.querySelector("#form").value.toLocaleLowerCase();
    //Nos muestre solo los elementos filtrados
    this.result = [];

    this.data.map( product => {
      const name = product.name.toLocaleLowerCase();

      if( name.indexOf(input) !== -1){
        this.result = [...this.result, product];
      }
    });

    if( this.result.length < 1) this.result = [{ name:'Producto', value:'No ha sido encontrado'}];
  }
}
