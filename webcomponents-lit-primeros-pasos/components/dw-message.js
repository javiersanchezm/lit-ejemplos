/*Importar
    LitElement. Clase que sirve como base para todos los customElements que
        se van a desarrollar apoyados en la librería, así poder mejorar el estándar
        de webComponents añadiendo funcionalidades como binding, aenlazar los atributos
        del componente con las propiedades
    html. Función para crear un template dentro de el. (Tagged String Template Literals)
    css. Se utiliza para dar estilos al componente, y se utiliza con una propiedad que se
        llama "static styles" y sirve para poder hacer la importación del css
        host. Selector para aplicar estilos que apliquen solo al componente
*/
import { LitElement, html, css} from "lit";

//Dentro de la clase va toda la funcionalidad y extender de litElement
//Lo recomendable es que se llame igual que el componente usando CamelCase
//Componente. Tiene una vista, estilos, propiedades que implementan la funcionalidad
class DwMessage extends LitElement{
    static styles = css`
        :host{
            display: block;
            border: 1px solid red;
            padding: 10px;
        }

        div{
            background-color: #fcc;
        }

        p{
            margin-bottom: 0;
        }
    `

    //Método donde se coloca la vista del componente, y renderiza el html
    render() {
        //interpreta el template literal como un HTML, es eficiente al renderizar
        //y actualizar el contenido
        return html`
                    <div>Hola Lit</div>
                    <p><b>Esto también va aquí !!</b></p>
                `;
    }
}

//Declarar el componente para que se cree el customElement y se pueda utilizar
//como una etiqueta html, para definirlo se realiza como dicta el estándar de los WebComponents
//Lo que hace internamente es traer la clase y utilizarla de manera que se implemente
//una nueva etiqueta que se va a llamar dw-message
customElements.define('dw-message', DwMessage);