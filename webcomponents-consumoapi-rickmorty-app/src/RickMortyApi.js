import { html, css, LitElement } from 'lit';
//Importar el componente
import './components/GetData';
//Importar los estilos
import style from './styles/RickMortyStyle';

import './components/ApiTemplate';

export class RickMortyApi extends LitElement {
  static get styles() {
    return [ style ];
  }

  //Variable para usar en cualquier parte de nuestra clase
  static get properties() {
    return {
      wiki: { type: Array },
    };
  }

  //Necesitamos escuchar el evento mediante el constructor
  //Lo recomendable sería mediante binding
  constructor() {
    super();

    //Al realizar una consulta a una API tiene un retraso y primero se renderiza
    //El componente, pasarle a wiki un arreglo vacío
    this.wiki = [];

    this.addEventListener( 'ApiData', (e) => {
      //Función que nos permita darle el formato para mostrar la info
      this._dataFormat(e.detail.data);
      //console.log(e.detail.data);
    });
  }

  _dataFormat( data ) {
    let characters = [];

    //Extraer solo la información que se necesita
    data["results"].forEach( (character) => {
        characters.push( {
          img: character.image,
          name: character.name,
          species: character.species,
          status: character.status
        });
    });

    this.wiki = characters;
  }

  render() {
    return html`
      <api-template></api-template>
      <get-data url="https://rickandmortyapi.com/api/character" method="GET"></get-data>
      <div class="container">
        ${this.dataTemplate}
      </div>
    `;
  }

  get dataTemplate() {
    return html `
      ${this.wiki.map( character => html`
        <div class="card">
          <div class="card-content">
            <h2>${character.name}</h2>
            <img src="${character.img}">
            <p>${character.species} | ${character.status}</p>
          </div>
        </div>
      `)}
    `;
  }
}
