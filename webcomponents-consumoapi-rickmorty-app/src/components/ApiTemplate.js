import { LitElement, html, css } from 'lit-Element';

export class ApiTemplate extends LitElement {
  static get styles() {
    return css`
      .container {
        text-align: center;
      }

      h1 {
        font-size: 100px;
      }

      .title {
        color: #24aa91;
      }

      p{
        font-size: 30px;
      }
    `;
  }

  render() {
    return html`
      <div class="container">
        <h1>The <strong class="title">Rick and Morty</strong> API</h1>
        <p class="title">LitElement</p>
      </div>
    `;
  }
}

customElements.define('api-template',ApiTemplate);
