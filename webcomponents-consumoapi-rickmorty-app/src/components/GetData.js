//Va a ir la parte lógica donde se le pasará la URL y nos regresará
//los datos de la consulta
import { LitElement } from "lit";

export class GetData extends LitElement {

  static get properties() {
    return {
      url: { type: String },
      method: { type: String },
    }
  }

  //Si colocamos getData en el constructor nos manda undefined
  //Tenemos que hacer uso del ciclo de vida de los componentes
  //Cuando el componente ya este inicializado y todas las propiedades
  //cargadas, se mandará a llamar a este método
  firstUpdated() {
    this.getData();
  }

  //Comunicación de hijo a padre para enviar información
  _sendData(data) {
    this.dispatchEvent(new CustomEvent('ApiData', {
      detail: { data }, bubbles: true, composed: true
    }));
  }

  getData() {
    fetch( this.url, { method: this.method })
      .then( resp => {
        if( resp.ok ) return resp.json();

        return Promise.reject(response);
      })
      //Si la respuesta llega bien mandarla a _sendData() +*****
      .then( (data) => { this._sendData(data); })
      .catch( (error) => { console.warn( 'Something went wrong', error) } );
  }
}

//Definir la etiqueta del componente
customElements.define("get-data", GetData);
